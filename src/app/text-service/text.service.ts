import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {SYNONYM_PARAMETR, SYNONYM_URL} from './text.constants';

@Injectable({
  providedIn: 'root'
})
export class TextService {
  private text$ = new BehaviorSubject<any>('');
  private selectedWord$ = new BehaviorSubject<any>([]);
  private synonym$ = new BehaviorSubject<any>('');
  private synonymApiUrl = `${SYNONYM_URL}${SYNONYM_PARAMETR}`;
  constructor(private http: HttpClient) {}
  getMockText() {
    return new Promise<string>(function (resolve) {
      resolve('A year ago I was in the audience at a gathering of designers in San Francisco. ' +
        'There were four designers on stage, and two of them worked for me. I was there to support them. ' +
        'The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. ' +
        'What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, ' +
        'that modern design problems were very complex. And we ought to need a license to solve them.');
    });
  }

  getText() {
    return this.text$.asObservable();
  }

  setText(text) {
    this.text$.next(text);
  }

  getSynonymList(word: string): Observable<any>  {
    return this.http.get(`${this.synonymApiUrl}${word}`);
  }

  getSelectedWord(): Observable<string> {
    return this.selectedWord$.asObservable();
  }

  setSelectedWord() {
    const selection = window.getSelection();
    this.selectedWord$.next(selection);
    return selection;
  }

  getSynonymWord() {
    return this.synonym$.asObservable();
  }
  setSynonymWord(synonym: string) {
    this.synonym$.next(synonym);
  }

}
