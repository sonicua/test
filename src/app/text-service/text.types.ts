export interface SynonymItem {
  word: string;
  score: number;
}

export type ResponseApi = Array<SynonymItem>;
