import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {TextService} from '../text-service/text.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnInit {
  private selectedWord: string;
  private synonymsList: BehaviorSubject<Array<string>> = new BehaviorSubject([]);
  constructor(private textService: TextService, private ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.textService.getSelectedWord().subscribe(
      (word) => {
        this.selectedWord = word.toString();
        this.textService.getSynonymList(this.selectedWord)
          .subscribe(response => this.synonymsList.next(response.map(item => item.word)));
      }
    );
  }

  chooseSynonym(synonym: string) {
    this.ngxSmartModalService.getModal('popUpModal').close();
    this.textService.setSynonymWord(synonym);
  }

}
