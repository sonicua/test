import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormatEnums} from './control-panel.enums';
import {TextService} from '../text-service/text.service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {INITIAL_COLOR} from './control-panel.constants';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent implements OnInit {
  private formatEnums = FormatEnums;
  private text;
  private selectedNode;
  private synonym: string;
  public initialColor = INITIAL_COLOR;
  constructor(private textService: TextService, private ngxSmartModalService: NgxSmartModalService) {
  }
  ngOnInit() {
    this.textService.getText().subscribe(textWrapper => this.text = textWrapper);
    this.textService.getSynonymWord().subscribe(synonym => {
      this.synonym = synonym;
      if (synonym) {
        this.changeSynonym(synonym);
      }
    });
  }

  handleFormatButtonClick(format: FormatEnums, value?: string) {
    document.execCommand(format, false, value);
  }

  handleSynonymsButtonClick() {
    this.selectedNode = window.getSelection();
    this.textService.setSelectedWord();
    this.ngxSmartModalService.getModal('popUpModal').open();
  }

  changeSynonym(synonym: string) {
    const selected = this.selectedNode;
    if (selected.rangeCount) {
      const range = selected.getRangeAt(0);
      range.deleteContents();
      range.insertNode(document.createTextNode(synonym));
    }
  }
}
