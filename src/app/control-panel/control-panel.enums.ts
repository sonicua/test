export enum FormatEnums {
  BOLD = 'bold',
  ITALIC = 'italic',
  UNDERLINE = 'underline',
  FORE_COLOR = 'foreColor'
}
