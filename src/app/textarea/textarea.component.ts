import {Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef} from '@angular/core';
import {TextService} from '../text-service/text.service';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextareaComponent implements OnInit {
  @ViewChild('textWrapper') textWrapper: ElementRef;
  constructor(private textService: TextService) { }

  ngOnInit() {
    this.textService.setText(this.textWrapper);
  }

}


